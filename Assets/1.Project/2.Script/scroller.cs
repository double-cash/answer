﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class scroller : MonoBehaviour
{


    public int Answer;

    public GameObject Content;
    public Vector2 content_pos;
    public Vector2 move_pos;
    public float speed;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        content_pos = Content.GetComponent<RectTransform>().anchoredPosition;
        move_pos = new Vector2(0, 168 * Answer);

        Content.GetComponent<RectTransform>().anchoredPosition = Vector2.MoveTowards(content_pos, move_pos, speed * Time.deltaTime);

    }

    private void OnTriggerEnter(Collider A)
    {
        Answer = int.Parse(A.name);
    }

}
