﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CheckAnswer : MonoBehaviour
{
    public int[] RealAnswer = new int[3];
    public scroller[] scroller = new scroller[3];
    public int[] Answer = new int[3];

    public Image Backgound, Answerbox;

    public Sprite[] Backgrounds;
    public Sprite[] Answerboxs;

    public GameObject AnswerButton, BackButton, HomeButton, CountTime;

    // Start is called before the first frame update
    void Start()
    {
        HomeButton.SetActive(true);
        BackButton.SetActive(false);
        AnswerButton.SetActive(true);
        CountTime.SetActive(false);
        Backgound.sprite = Backgrounds[0];
        Answerbox.sprite = Answerboxs[0];
    }

    // Update is called once per frame
    void Update()
    {
        Answer[0] = scroller[0].Answer;
        Answer[1] = scroller[1].Answer;
        Answer[2] = scroller[2].Answer;
    }

    public void Check()
    {

        bool a1 = ((IList)Answer).Contains(RealAnswer[0]);
        bool a2 = ((IList)Answer).Contains(RealAnswer[1]);
        bool a3 = ((IList)Answer).Contains(RealAnswer[2]);

        AnswerButton.SetActive(false);

        if (a1 && a2 && a3)
        {
            TrueAnswer();
        }
        else
        {
            FalseAnswer();
        }

    }

    private void TrueAnswer()
    {
        HomeButton.SetActive(true);
        CountTime.SetActive(true);
        Backgound.sprite = Backgrounds[1];
        Answerbox.sprite = Answerboxs[1];
        StartCoroutine(Count());
    }

    private void FalseAnswer()
    {
        HomeButton.SetActive(true);
        BackButton.SetActive(true);
        CountTime.SetActive(true);
        Backgound.sprite = Backgrounds[2];
        Answerbox.sprite = Answerboxs[2];
        StartCoroutine(Count());
    }

    public void Answer_Button()
    {

    }

    public void Back_Button()
    {
        Application.LoadLevel(1);
    }

    public void Home_Button()
    {
        Application.LoadLevel(0);
    }

    public IEnumerator Count()
    {
        yield return new WaitForSeconds(5);
        Home_Button();
    }

}
