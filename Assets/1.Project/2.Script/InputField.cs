﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InputField : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (GetComponent<UnityEngine.UI.InputField>().text.Length > 1)
            GetComponent<UnityEngine.UI.InputField>().text = A(GetComponent<UnityEngine.UI.InputField>().text);
    }

    public string A(string text)
    {
        return text.Substring(text.Length - 1);
    }

}
